import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 's-uz1wq+-sxw#*3(^gdhj0vpys-dejnu#zwy7rceo&m@np3fmg'
DEBUG = True

ALLOWED_HOSTS = ['*', '8055.ru', 'www.8055.ru']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'front.apps.FrontConfig',
]
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'cleaning.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages'
                
            ]
        },
    },
]

WSGI_APPLICATION = 'cleaning.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, "media")

# Use Telegramm
TELEGRAM_MESSAGES = True
# Use SMS
SMS_MESSAGES = True

try:
    from .local_settings import *
except ImportError:
    from .prod_settings import *

# Link for sending messages ti telegram when dumper falls. !!NO SLASH AT END OF LINK!!
# move it to model Telegram
# TELEGRAM_LINK = 'api.telegram.org/bot434976718:AAFsllg5_Nkjf4L3yUrZEZaTBJCcKpnjcqM'

EMAIL_HOST = 'smtp.mail.ru'
EMAIL_PORT = 2525
EMAIL_HOST_USER = 'info@smart-mats.com'
EMAIL_HOST_PASSWORD = 'smart_mats_info'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'info@smart-mats.com'
SERVER_EMAIL = 'info@smart-mats.com'

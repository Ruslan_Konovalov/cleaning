from django.shortcuts import render
from django.http import JsonResponse
from django.core import serializers
from cleaning.settings import TELEGRAM_MESSAGES
import functools
from .models import PlaceType, CleaningType, Price, Request, Telegram


def home(request):
    """Render main page"""
    places = PlaceType.objects.all()
    types = CleaningType.objects.all()
    return render(request, 'front/index.html', {'places': places, 'types': types})


def price(request):
    price = Price.objects.all()
    price_map = list(map(lambda x: {
                     'place': x.place_type_id, 'type': x.clean_type_id, 'sq': x.square, 'price': x.price}, price))
    return JsonResponse(price_map, safe=False)


def request(request):
    cost = request.POST.get('cost')
    clean_type = request.POST.get('type')
    place_type = request.POST.get('place')
    date = request.POST.get('date')
    time = request.POST.get('time')
    name = request.POST.get('name')
    phone = request.POST.get('phone')
    address = request.POST.get('adress')
    clean = CleaningType.objects.get(id=clean_type)
    request = Request(
        name=name, 
        clean_type=clean,
        phone=phone, 
        date="{} {}".format(date, time), 
        cost=cost,
        address=address
    )
    request.save()
    if(TELEGRAM_MESSAGES):
        tlgs = Telegram.objects.all()
        for tlg in tlgs:
            tlg.send_message("{}-{},{},{},{},{}".format(name, phone, clean.name, address, cost, "{} {}".format(date, time)))
    # if(SMS_MESSAGES):
    #     smss = Twilio.objects.all()
    #     for sms in smss:
    #         sms.send_message("{}-{},{},{},{},{}".format(name, phone, clean.name, address, cost,  "{} {}".format(date, time)))
    return JsonResponse({'status': 'OK'})

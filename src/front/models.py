from django.db import models
from cleaning.settings import TELEGRAM_MESSAGES, SMS_MESSAGES
# from twilio.rest import Client
import requests

# Create your models here.


class PlaceType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class CleaningType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Price(models.Model):
    place_type = models.ForeignKey(PlaceType, on_delete=models.CASCADE)
    clean_type = models.ForeignKey(CleaningType, on_delete=models.CASCADE)
    square = models.IntegerField()
    price = models.DecimalField(decimal_places=2, max_digits=15)

    def __str__(self):
        return "{},{},{}".format(self.place_type.name, self.clean_type.name, self.square)


class Request(models.Model):
    clean_type = models.ForeignKey(CleaningType, on_delete=models.CASCADE)
    phone = models.TextField(max_length=20)
    name = models.TextField(max_length=100)
    cost = models.DecimalField(decimal_places=2, max_digits=15)
    date = models.TextField(max_length=20)
    address = models.TextField(max_length=100, default="")


class Telegram(models.Model):
    link = models.TextField(max_length=100)
    chat_id = models.TextField(max_length=100)

    def send_message(self, text):
        if TELEGRAM_MESSAGES:
            print('telegramm {}'.format(text))
            method = 'sendMessage'
            url = "http://{}/{}?chat_id={}&text={}".format(
                self.link, method, self.chat_id, text)
            requests.post(url)

#
# class Twilio(models.Model):
#     account_sid = models.TextField(max_length=100)
#     auth_token = models.TextField(max_length=100)
#     from_phone = models.TextField(max_length=100)
#     to_phone = models.TextField(max_length=100)
#
#     def send_message(self, text):
#        self.send_message_to(text, self.to_phone)
#
#     def send_message_to(self, text, phone):
#         if SMS_MESSAGES:
#             print('sms {}'.format(text))
#             client = Client(self.account_sid, self.auth_token)
#             try:
#                 result = client.messages.create(
#                     body=text, from_=self.from_phone, to=phone)
#                 print(result)
#             except:
#                 print('sms send with error')
#                 pass

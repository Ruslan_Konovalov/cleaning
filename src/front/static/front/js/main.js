var x, i, j, selElmnt, a, b, c, arrow, additionalCost = 0;
for ($(document).ready(function () {
    $(".calculator__step-1").click(function () {
        $(".calculator").removeClass("calculator_step2"), $(".calculator").addClass("calculator_step1")
    }),
        $(".calculator__step-2").click(function () {
            $(".calculator").removeClass("calculator_step1"), $(".calculator").addClass("calculator_step2")
        }),
        $(".quantity input").on("input change paste", function () {
            // var e = this.value.replace(/[^0-9\-]/, "");
            // e = e < 0 ? 0 : +e, $(this).val(e)
            calcACost()
        }),
        $(".quantity > button").on("click", function () {
            var e = $(this).closest(".quantity").find("input"),
                t = parseInt(e.val()) || 0;
            s = parseInt($(".additional-offers__item-price", $(this).closest(".additional-offers__item-group")).text())
            additionalCost -= t * s
            $(this).hasClass("quantity__minus") && (t -= 1), $(this).hasClass("quantity__plus") && (t += 1), t = t < 0 ? 0 : t, e.val(t).change()
            additionalCost += t * s
            $("#additional-offers__total_value").text(additionalCost)
            calcACost()
        }),
        $(".checkbox input").on("change", () => {
            calcACost()
        }),
        $("#calculator__form-next").click(function () {
            $(".calculator").toggleClass("calculator_step1"), $(".calculator").toggleClass("calculator_step2")
        }),
        $("#calculator__form-submit").click(function () {
            $(".calculator__form").find(".calculator__form-submit-button").click()
        }),
        $(".calculator__form").on('submit', function (e) {
            event.preventDefault();
            data = $(this).serializeArray().reduce((x, i) => {
                x[i.name] = i.value
                return x
            }, {})
            data['cost'] = $("#calulator__total__value").text()
            data['csrfmiddlewaretoken'] = $("input[name=csrfmiddlewaretoken]").val()
            console.log(data)
            $.ajax("/request", { method: "POST", data }).done(_ => {
                $("#ex1").modal()
            })
        })
}),
    jQuery(function (e) {
        e("input[name='phone']").mask("+7(999) 999 9999", {
            placeholder: " "
        }), e("input[name='area']").mask("9?99999", {
            placeholder: " "
        })
    }),
    $("input[name='date']").datepicker({}),
    x = document.getElementsByClassName("custom-select"), i = 0; i < x.length; i++) {
    for (selElmnt = x[i].getElementsByTagName("select")[0], (a = document.createElement("DIV")).setAttribute("class", "select-selected"), a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML, (arrow = document.createElement("IMG")).setAttribute("src", "/static/front/images/down-chevron.svg"), arrow.setAttribute("class", "select-selected__arrow"), a.appendChild(arrow), x[i].appendChild(a), (b = document.createElement("DIV")).setAttribute("class", "select-items select-hide"), j = 1; j < selElmnt.length; j++)(c = document.createElement("DIV")).innerHTML = selElmnt.options[j].innerHTML, c.setAttribute('value-name', selElmnt.name), c.setAttribute('value-id', selElmnt.options[j].value), c.addEventListener("click", function (e) {
        var t, l, a, s, c;
        for (s = this.parentNode.parentNode.getElementsByTagName("select")[0], c = this.parentNode.previousSibling, l = 0; l < s.length; l++)
            if (s.options[l].innerHTML == this.innerHTML) {
                for (s.selectedIndex = l, c.innerHTML = this.innerHTML, t = this.parentNode.getElementsByClassName("same-as-selected"), a = 0; a < t.length; a++) t[a].removeAttribute("class");
                this.setAttribute("class", "same-as-selected");
                calcACost()
                break
            } c.click()
    }), b.appendChild(c);
    x[i].appendChild(b), a.addEventListener("click", function (e) {
        e.stopPropagation(), closeAllSelect(this), this.nextSibling.classList.toggle("select-hide"), this.classList.toggle("select-arrow-active")
    })
}
function calcACost() {
    selectors = $(".same-as-selected")
    area = $(".quantity input").val()
    if (selectors.length == 2 && area > 0) {
        $.ajax('/price').done(function (data) {
            place = $(selectors[0]).attr("value-id")
            type = $(selectors[1]).attr("value-id")
            price_arr = data.filter(x => x.place == place && x.type == type && x.sq <= area).sort((a, b) => b.sq - a.sq)
            if (price_arr.length > 0) {
                price = parseFloat(price_arr[0].price)
            } else {
                price_arr = data.filter(x => x.place == place && x.type == type && x.sq > area).sort((a, b) => a.sq - b.sq)
                if (price_arr.length > 0) {
                    price = parseFloat(price_arr[0].price)
                }
            }
            console.log('Current price ', price || 0)
            cost = (price || 0) * area
            if ($(".checkbox input")[0].checked) {
                cost += 1000
            }
            cost += additionalCost
            $("#calulator__total__value").text(cost)
        })
    }
}
function closeAllSelect(e) {
    var t, l, a, s = [];
    for (t = document.getElementsByClassName("select-items"), l = document.getElementsByClassName("select-selected"), a = 0; a < l.length; a++) e == l[a] ? s.push(a) : l[a].classList.remove("select-arrow-active");
    for (a = 0; a < t.length; a++) s.indexOf(a) && t[a].classList.add("select-hide")
}
document.addEventListener("click", closeAllSelect);
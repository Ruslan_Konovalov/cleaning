from django.contrib import admin
from .models import Price, CleaningType, PlaceType, Request, Telegram

admin.site.register(Price)
admin.site.register(PlaceType)
admin.site.register(CleaningType)
admin.site.register(Request)
admin.site.register(Telegram)
# admin.site.register(Twilio)
# Register your models here.

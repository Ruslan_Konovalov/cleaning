from django.urls import path
from . import views


app_name = 'front'
urlpatterns = [
    path('', views.home, name='home'),
    path('price', views.price, name='price'),
    path('request', views.request, name='request')
]

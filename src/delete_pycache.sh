#!/bin/bash

find . | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf;

echo "__pycache__ removed";

find . -name ".DS_Store";

find . -name ".DS_Store" -exec rm -f {} \;

echo ".DS_Store removed";
